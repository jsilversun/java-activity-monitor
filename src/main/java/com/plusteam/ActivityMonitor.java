package com.plusteam;

import javax.imageio.ImageIO;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import oshi.software.os.OperatingSystem;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem.ProcessSort;
import oshi.util.FormatUtil;
import oshi.SystemInfo;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

public class ActivityMonitor {
    SystemInfo si;
    HardwareAbstractionLayer hal;
    OperatingSystem os;

    public ActivityMonitor() {
        // Setup keyboard and mouse listener
        try {
            GlobalScreen.registerNativeHook();
            Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
            logger.setLevel(Level.OFF);
            logger.setUseParentHandlers(false);
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        MouseListener mouseListener = new MouseListener();
        KeyListener keyListener = new KeyListener();
        GlobalScreen.addNativeMouseListener(mouseListener);
        GlobalScreen.addNativeMouseMotionListener(mouseListener);
        GlobalScreen.addNativeKeyListener(keyListener);

        // Setup processes listener
        this.si = new SystemInfo();
        this.hal = si.getHardware();
        this.os = si.getOperatingSystem();
    }

    public void takeScreenshot(String location) {
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        BufferedImage screen = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        File file = new File(location);
        try {
            ImageIO.write(screen, "jpg", file);
            System.out.println("Screenshot successfully saved on: " + location);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printProcesses() {
        OperatingSystem os = this.os;
        GlobalMemory memory = this.hal.getMemory();
        System.out.println("Processes: " + os.getProcessCount() + ", Threads: " + os.getThreadCount());
        // Sort by highest CPU
        List<OSProcess> procs = Arrays.asList(os.getProcesses(5, ProcessSort.CPU));

        System.out.println("   PID  %CPU %MEM       VSZ       RSS Name");
        for (int i = 0; i < procs.size() && i < 5; i++) {
            OSProcess p = procs.get(i);
            System.out.format(" %5d %5.1f %4.1f %9s %9s %s%n", p.getProcessID(),
                    100d * (p.getKernelTime() + p.getUserTime()) / p.getUpTime(),
                    100d * p.getResidentSetSize() / memory.getTotal(), FormatUtil.formatBytes(p.getVirtualSize()),
                    FormatUtil.formatBytes(p.getResidentSetSize()), p.getName());
        }
    }
}
