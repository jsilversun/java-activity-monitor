package com.plusteam;

import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import oshi.SystemInfo;

public class MouseListener implements NativeMouseInputListener {
    Boolean showMouseMovement = false;

    public void nativeMouseClicked(NativeMouseEvent e) {
        this.showMouseMovement = !showMouseMovement;
        if(this.showMouseMovement){
            System.out.println("Mouse Clicked: " + e.getClickCount());
        }
    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {

    }

    public void nativeMouseMoved(NativeMouseEvent e) {
        if(this.showMouseMovement){
            System.out.println("Mouse Moved: " + e.getX() + ", " + e.getY());
        }
    }

    @Override
    public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {

    }

}
