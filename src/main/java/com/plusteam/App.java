package com.plusteam;
import oshi.SystemInfo;

import java.io.File;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String DEFAULT_FILE_LOCATION = System.getProperty("user.home") + File.separator + "file.jpg";

        ActivityMonitor activityMonitor = new ActivityMonitor();
        System.out.println("1. Take screenshot");
        activityMonitor.takeScreenshot(DEFAULT_FILE_LOCATION);
        System.out.println("2. Print top 5 processes with higher CPU usage");
        activityMonitor.printProcesses();
        System.out.println("3. Click anything to start recording mouse activity (Click again to turn it off)");
        System.out.println("4. Type 'K' to start recording mouse activity (Press it again to turn it off)");
    }
}
