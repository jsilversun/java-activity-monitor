# Demo of activity monitor using JAVA
This program will show you how to:

1. Take a fullscreen screenshot
2. Monitor processes and CPU/memory usage
3. Use native global listeners for keyboard and mouse